# -*- coding: utf-8 -*-
from argparse import ArgumentParser
import os
import gensim
from sklearn.linear_model import LinearRegression
from collections import defaultdict


def load_dir(inp_dir, data_type='txt'):
    out = defaultdict(list)
    listing = [f for f in os.listdir(inp_dir) if f.endswith(data_type)]
    for infile in listing:
        name = infile.split('.txt')[0]
        data = load(os.path.join(inp_dir, infile))
        for b, c in data:
            out[name].append((b, c))
    return out


def load(filepath):
    out = []
    for line in open(filepath, 'r'):
        cols = line.strip().split('\t')
        b, c = cols[0], cols[1]
        out.append((b, c))
    return out


if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument('-d', '--traindata-path', help='Train Data path (single file)', required=True)
    parser.add_argument('-t', '--testdata-path', help='Test Data path (single file)', required=True)
    parser.add_argument('-wv', '--embeddings', help='Embeddings', required=True)
    parser.add_argument('-o', '--output-folder', help='Output folder', required=True)

    args = parser.parse_args()

    train_dataset = load_dir(args.traindata_path)
    test_dataset = load_dir(args.testdata_path)
    print('Loading embeddings')
    model = gensim.models.KeyedVectors.load_word2vec_format(args.embeddings, binary=True)
    #model = gensim.models.Word2Vec.load(args.embeddings)

    for lf in train_dataset:
        print('Processing lf ', lf)
        B = []
        C = []
        for b, c in train_dataset[lf]:
            B.append(b)
            C.append(c)

        B_w = []
        C_w = []

        for b, c in zip(B, C):
            if b in model.wv.vocab and c in model.wv.vocab:
                B_w.append(model[b])
                C_w.append(model[c])

        print('Training with ', len(B_w), ' pairs')
        # Linear Regression Model
        clf = LinearRegression()
        clf.fit(B_w, C_w)

        test_data = defaultdict(list)

        for b, c in test_dataset[lf]:
            test_data[b].append(c)

        print('Applying model on ', len(test_data), ' bases')
        with open(os.path.join(args.output_folder, lf + '_gold.txt'), 'w') as outf_gold:
            with open(os.path.join(args.output_folder, lf + '_predictions.txt'), 'w') as outf_pred:
                for b in test_data:
                    if b in model.wv.vocab:
                        outf_gold.write(b + '\t' + '\t'.join([goldcol for goldcol in test_data[b]]) + '\n')
                        nns = model.most_similar(positive=clf.predict([model[b]]))
                        # print('For lexical function: ', lf, ' | For test base: ', b, ' | With gold collocates: ', test_data[b])
                        outf_pred.write(b + '\t' + '\t'.join([predcol for predcol, _ in nns]) + '\n')
                # for n, _ in nns:
                #	print(n)
                # print('---')
            # else:
            # print('Skipping ', b, ' | Not in embeddings vocab')
            # print('---')
