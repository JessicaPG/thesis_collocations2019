
---

## TODO tasks
---

## Finished tasks

- Download Wikipedia Corpus (Dump 28 nov)
- Clean corpus with this [script](https://github.com/bwbaugh/wikipedia-extractor)
     - [EN corpus](https://drive.google.com/open?id=15X1JFljTR_U56sRvbkY_O7BmMl7sgC0r)
     - [FR corpus](https://drive.google.com/open?id=12XZL1UFZixBAG5lxgPUU8w3UuUk27cPc)
     - [ES corpus](https://drive.google.com/open?id=1QlV6X_7I2t_4DmH5IPpGg5yQxmRp3LPc)
- Train vector embeddings (train_word2vec.py)
    - [EN vw](https://drive.google.com/open?id=1D62WcAjoIgh1y2FGrL8Ft0EPPSL4GKh9)
    - [FR vw](https://drive.google.com/open?id=1J4go-h7J20-W71SMCFwYttYUjuS2KClI)
    - [ES vw](https://drive.google.com/open?id=1poh5cfqbFYOPVFOqDp1D1ncOFfXzB9HJ)
 
